<br>
####Select elements
Select elements can be styled by targeting them with the included FancySelect.js plugin. 

    $('.class').fancySelect();

The styles for all fancy select dropdowns can be located in `_select.scss` and the variables contained at the top of the file should suit most styling needs.

<br>
####Radio & Checkboxes
TODO: Include pretty checkable in to base camp

<br>
####Validation
TODO: Put in jQuery validate into basecamp

<br>
####PHP Mailer 
This is a simple PHP mail function that extracts the parameters sent to it in a post request and sends an email with headers, message and reply-to fields.

This should be used only for testing deivce during developmeto make sure that your forms are working correctly and so as to test your response error & success returns.  