$(function() {

	// Select element styling
	$('select').fancySelect();

	// Radio & checkbox styling
	for (var i = $('.radio--button').length - 1; i >= 0; i--) {
		$($('.radio--button')[i]).radioBox({
			states: {
				normal  : 'checkbox',
				checked : 'checkbox-checked'
			},
			width: 32,
			height: 32
		});
	};

});