<br>
####Console fallback
Console errors within older browers is averted by including `console.log.js`

<br>
####Placeholders
To provide faux placeholders for older browsers include `placeholders.js`

<br>
####Utilities
For a more aesthetically pleasing way of writing html templates you can insert each line into a javascript object property. This just looks nicer.

    tmpl = {
            1:'<div>',
            2:' <h1>This is the template</h1>',
            3:' <p>This is a paragraph</p>',
            4:'</div>'
        }

    console.log(render(tmpl));

Will output as:

    <div>
        <h1>This is the template</h1>
        <p>This is a paragraph</p>
    </div>

