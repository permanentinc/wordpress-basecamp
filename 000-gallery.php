<?php /* Template name: Gallery */ ?>

<?php include 'header.php'; ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<?php the_content(); ?>
	<?php
	$images =& get_children( array (
		'post_parent' => $post->ID,
		'post_type' => 'attachment',
		'post_mime_type' => 'image'
		));

	if ( empty($images) ) {
	} else {
		foreach ( $images as $attachment_id) {
			$thumbnail = wp_get_attachment_image_src($attachment_id->ID, 'thumbnail');
			$fullsize = wp_get_attachment_image_src($attachment_id->ID, 'full');
			echo $fullsize[0];
			echo $thumbnail[0];
		}
	}
	?>
<?php endwhile; ?>

<?php include 'footer.php';?>
