#Basecamp#
Automated work flow documentation

Node.js
-------

The first time you use this boilerplate, you will need to run the following within the context of this folder to download any required dependencies from the NPM registry:

    $ npm install

If you are using OSX and encounter any security issues, you may need to run the same command using administrative privelges:

    $ sudo npm install


Gulp tasks
-------

This will run the default gulp task which will initially compile all of the *.png sprite files and then continue to watch the style sheets for changes and compile them accordingly

    $ gulp

This is the same as above apart from the fact that it also instatiates a hosting environment for you to view you apllication on. Any style or markup changes that you make will be reflected in the page without having to refresh

    $ gulp serve

This command will only compile the SASS files once without watching continuously. 

    $ gulp sass


Style Guide
-------

<br><br>