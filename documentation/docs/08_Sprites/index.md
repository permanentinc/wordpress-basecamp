<br>
####Configuration
Configuration is via a JSON file called `svg-sprite-config.json` in the root path of the theme. It specifies where to look for source sprites, where to output the final sprites, and other options such as XML header and footer text.

    {
        "source-path"          : "sprites/svg/*.sprite",    // required; multiple nested folders can be expressed like "images/svg/*/*.sprite"
        "output-path"          : "sprites/svg-sprites.svg", // required
        "before"               : "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"0\" height=\"0\" overflow=\"hidden\">", // optional
        "after"                : "</svg>",                  // optional
        "output-mode"          : "normal"                   // optional; can be 'normal' or 'compressed'
        "defs-open-tag"        : "<defs>"                   // optional; opening tag for the defs header section
        "defs-close-tag"       : "</defs>"                  // optional; closing tag for the defs header section
        "group-open-tag"       : "<group id=\"$id\">"       // optional; opening tag for each group; $id is replaced by the svg id
        "group-close-tag"      : "</group>"                 // optional; closing tag for each group
        "html-output-path"     : "../../templates/Sprites"  // optional; output path for html templates
        "html-output-extension": "ss"                       // optional; file extension for template files
    }

You can also specify multiple source paths, such as:

    "source-path": ["sprites/svg/*.sprite", "image/svg/*/*.sprite"] // Also include all sprites in subfolders of sprites/svg

The `combine-svg-sprites` script will look for an `svg-sprite-config.json` file in the local folder, but you can manually specify one like so:

    > php combine-svg-sprites.php --config-path=svg/svg-sprite-config.json

The syntax is the same for the Node.js script.

<br>
####PHP

Use the PHP script to combine your files:

    > php combine-svg-sprites.php

<br>
####Node.js

The first time you use this, run the following within this folder to download any dependencies:

    $ npm install

Then use the `combine-svg-sprites` script to merge your files:

    > node combine-svg-sprites.js

