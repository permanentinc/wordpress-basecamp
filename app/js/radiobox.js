//---------------------------------------------------------------------------------------------
//
// Radio and checkbox styling plugin
//
//---------------------------------------------------------------------------------------------


;(function ( $, window, document, undefined ) {
	// 'use strict';

	var pluginName = 'radioBox';
	var dataPlugin = 'plugin_' + pluginName;
	var  defaults = {
		states: {
			normal  : 'checkbox',
			checked : 'checkbox-checked'
		},
		width: 32,
		height: 32
	};

	var addCheckableEvents = function (element) {

		if (window.ko) {

			$(element).on('change', function(e) {

				e.preventDefault();

				if (e.originalEvent === undefined) {

					var clickedParent = $(this).closest('.radioBox'),
					fakeCheckable = $(clickedParent),
					isChecked = fakeCheckable.hasClass('checked');

					if (isChecked === true) {

						fakeCheckable.addClass('checked');

					} else {

						fakeCheckable.removeClass('checked');

					}

				}

			});

		}

		element.parent().on('touchstart click', function(e){

			e.preventDefault();

			var clickedParent = $(this).closest('.radioBox');
			var input = clickedParent.find('input');
			var	fakeCheckable = clickedParent;

			if (fakeCheckable.hasClass('disabled') === true) {

				return;

			}

			if (input.prop('type') === 'radio') {

				$('input[name="' + input.attr('name') + '"]').each(function(index, el){

					$(el).prop('checked', false).parent().removeClass('checked');

				});

			}

			if (window.ko) {

				ko.utils.triggerEvent(input[0], 'click');

			} else {

				if (input.prop('checked')) {

					input.prop('checked', false).change();

				} else {

					input.prop('checked', true).change();

				}

			}

			fakeCheckable.toggleClass('checked');

		});

		element.find('.icon-img').on('keyup', function(e){

			if (e.keyCode === 32) {

				$(this).click();

			}

		});

	};

	var Plugin = function ( element ) {
		this.element = element;
		this.options = $.extend( {}, defaults );
	};

	Plugin.prototype = {
		init: function ( options ) {

			$.extend( this.options, options );

			var el = $(this.element);

			el.parent().addClass('radioBox');

			el.css('display', 'none');

			var classType = el.data('type') !== undefined ? el.data('type') : el.attr('type');

			var containerClasses = ['radioBox-' + classType].join(' ');

			el.wrap('<div class="radioBox-wrap ' + containerClasses + '" style="width:' + this.options.width + 'px;height:' + this.options.height + 'px;"></div>').parent().html();

			for (var state in this.options.states) {

				var spriteTemplate = {
					1:'<div class="icon-img icon-' +  this.options.states[state] + ' '+ state + '-state">',
					2:'	<svg class="icon-svg" viewBox="0 0 ' + this.options.width + ' ' + this.options.height + '">',
					3:' 	<use xlink:href="#' +  this.options.states[state] + '"></use>',
					4:'	</svg>',
					5:'</div>'
				}

				function spriteInject(a){
					var c='';
					for (var b in a) {
						c += a[b] + '\n';
					}
					return c;
				}

				el.parent().append(spriteInject(spriteTemplate));

			}

			addCheckableEvents(el.parent());

		},
		check: function(){

			console.log('Clicked');

			if ($(this.element).prop('type') === 'radio') {

				$('input[name="' + $(this.element).attr('name') + '"]').each(function(index, el){

					$(el).prop('checked', false).attr('checked', false).parent().removeClass('checked');

				});

			}

			$(this.element).prop('checked', true).attr('checked', true).parent().addClass('checked');
		},
		uncheck: function () {

			$(this.element).prop('checked', false).attr('checked', false).parent().removeClass('checked');

		}


	}

	$.fn[ pluginName ] = function ( arg ) {
		var args, instance;
		if (!( this.data( dataPlugin ) instanceof Plugin )) {
			this.data( dataPlugin, new Plugin( this ) );
		}
		instance = this.data( dataPlugin );
		instance.element = this;
		if (typeof arg === 'undefined' || typeof arg === 'object') {
			if ( typeof instance.init === 'function' ) {
				instance.init( arg );
			}
		} else if ( typeof arg === 'string' && typeof instance[arg] === 'function' ) {
			args = Array.prototype.slice.call( arguments, 1 );
			return instance[arg].apply( instance, args );
		} else {
			$.error('Method ' + arg + ' does not exist on jQuery.' + pluginName);
		}
	};

}(jQuery, window, document));