Download the latest version of basecamp fom gitlab
    https://gitlab.com/permanentinc/basecamp

The first time you use this, run the following within this folder to download the required dependencies:

    $ npm install

If you are using OSX you may need to run the same command using administrative privelges:

    $ sudo npm install

<br>

####Gulp tasks

This will run the default gulp task which will initially compile all of the PNG & SVG sprites and then continue to watch the style sheets for changes and compile them accordingly

    $ gulp

This is the same as above apart from the fact that it also instatiates a hosting environment for you to view you apllication on. Any style or markup changes that you make will be reflected in the page without having to refresh

    $ gulp serve

This command will only compile the SASS files once without watching continuously. 

    $ gulp sass



