<br>
####Gulp tasks

This will run the default gulp task which will initially compile all of the *.png sprite files and then continue to watch the style sheets for changes and compile them accordingly

    $ gulp

This is the same as above apart from the fact that it also instatiates a hosting environment for you to view you apllication on. Any style or markup changes that you make will be reflected in the page without having to refresh

    $ gulp serve

This command will only compile the SASS files once without watching continuously. 

    $ gulp sass


