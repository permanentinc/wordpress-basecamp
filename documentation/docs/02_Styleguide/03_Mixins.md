Includeed are several mixins that are designed to help keep code clean and avoid repeating commonly used expressions. 

<br>
####Pseudo
When creating  pseudo elements I tend to forget to include content attribute so this handy mixing helps me get it in everytime. `@include pseudo`

<br>
####Clearfix
A micro clearfix hack. Either use it as a mixin with `@include clearfix` or as a class with `.clearfix`

<br>
####Tranistion timings
A set of predefined, commonly used transition timings and their cubic bezier representation.

`$easeInQuad`
`$easeOutQuad`
`$easeInOutQuad`
`$easeInCubic`
`$easeOutCubic`
`$easeInOutCubic`
`$easeInQuart`
`$easeOutQuart`
`$easeInOutQuart`
`$easeInQuint`
`$easeOutQuint`
`$easeInOutQuint`
`$easeInSine`
`$easeOutSine`
`$easeInOutSine`
`$easeInExpo`
`$easeOutExpo`
`$easeInOutExpo`
`$easeInCirc`
`$easeOutCirc`
`$easeInOutCirc`
`$easeInBack`
`$easeOutBack`
`$easeInOutBack`

<br>
####Rem sizing with fallbacks

This is used throughout your application to define font sizes, margins and more. On older browsers it will fall back to its pixel equivalent.

`@include rem(font-size, 3rem);`<br>
`@include rem(line-height, 4rem);`<br>
`@include rem(margin, 0 auto 1.9rem);`<br>
