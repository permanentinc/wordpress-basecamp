<?php /* Template name: Contact */ ?>  

<?php include 'header.php'; ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<?php the_content(); ?>

<?php endwhile; ?>

<?php include 'footer.php';?>