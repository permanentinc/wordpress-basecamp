</div>

<div class="push"></div>

</div>
<footer class"sticky">
  <div class="row">
    <p class="grid_3">&nbsp;</p>
    <p class="grid_6"><a href="mailto:info@theateam.co.nz">INFO@BLUESTEEL.CO.NZ</a><br/ >© 2014 BLUE STEEL.<br />All RIGHTS RESERVED.</p>
    <p class="grid_3">&nbsp;</p>
  </div>
</footer>
<!-- Main javascript calls -->
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.min.js"></script> <!-- Replace with a CDN when going live -->
<!-- Useful plugins that are often used -->
<script src="<?php bloginfo('template_directory'); ?>/js/thirdparty.combined.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/sc-player.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/soundcloud.player.api.js"></script>
<!-- Include all of your main custom functions in this file -->
<script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>
<script type="text/javascript">
  $(document).ready(function(){

    $('.home-link').on('click',function(e){
      e.preventDefault();
      var el = $(this),
      link = el.data('link');
      window.location.href = "http://permanentinc.co.nz/bluesteel/"+link;
    })

  $('.chosen').fancySelect(); // Select styling

  for (var i = $('.prettyCheckbox').length - 1; i >= 0; i--) {$($('.prettyCheckbox')[i]).prettyCheckable();} // Checkbox styling

  for (var i = $('.prettyRadio').length - 1; i >= 0; i--) {$($('.prettyRadio')[i]).prettyCheckable();} // Radio styling

  $('.videos').fitVids(); //Responsive youtube & vimeo embeds

  $('.fancybox').fancybox({helpers:{overlay:{locked: false}}}); //Lightbox on images - helper to stop page from bouncing to top

  window.setInterval(function(){
    $('section.listen .sc-trackslist li.active .progress-bar').width(0);
    var progress = $('.sc-played').width();
    if(progress<665){$('section.listen .sc-trackslist li.active .progress-bar').width(progress);}
  },20);

  $(".news-image").each(function(index, value) {
   $(this).backstretch($(this).data('image'));
 });

});
</script>
<!-- asynchronous google analytics change the UA-XXXXX-X to be your site's ID -->
<script>
 var _gaq = [['_setAccount', 'UA-XXXXX-X'], ['_trackPageview']];
 (function(d, t) {
  var g = d.createElement(t),
  s = d.getElementsByTagName(t)[0];
  g.async = true;
  g.src = '//www.google-analytics.com/ga.js';
  s.parentNode.insertBefore(g, s);
})(document, 'script');
</script>
</body>
</html>