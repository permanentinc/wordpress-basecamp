<!DOCTYPE html>
<html>
<head>
  <meta charset=utf-8 />
  <meta name="viewport" content="width=device-width,initial-scale=1" />
  <meta name="description" content="description">
  <title>Fame-Work</title>
  <!-- Open Graph -->
  <meta property="og:title" content="Page title goes here" />
  <meta property="og:description" content="Page description goes here" />
  <meta property="og:url" content="http://www.your-website.co.nz/" />
  <meta property="og:image" content="http://www.your-website.co.nz/path/to/og-image" />
  <meta property="og:site_name" content="Website title goes here" />
  <meta property="og:type" content="website" />
  <!-- Favicons/Icons -->
  <link href="<?php bloginfo('template_directory'); ?>/favicon.ico" rel="icon" type="image/x-icon" />
  <!-- For iPad with high-resolution Retina display running iOS ≥ 7: -->
  <link rel="apple-touch-icon-precomposed" sizes="152x152" href="favicon-152.png">
  <!-- For iPad with high-resolution Retina display running iOS ≤ 6: -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="favicon-144.png">
  <!-- For iPhone with high-resolution Retina display running iOS ≥ 7: -->
  <link rel="apple-touch-icon-precomposed" sizes="120x120" href="favicon-120.png">
  <!-- For iPhone with high-resolution Retina display running iOS ≤ 6: -->
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="favicon-114.png">
  <!-- For first- and second-generation iPad: -->
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="favicon-72.png">
  <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
  <link rel="apple-touch-icon-precomposed" href="favicon-57.png">
  <!-- Windows live tiles -->
  <meta name="msapplication-config" content="ieconfig.xml" />
  <!-- Stylesheets -->
  <link rel="stylesheet" media="screen" href="<?php bloginfo('template_directory'); ?>/stylesheets/css/style.css" />
  <!-- Initial javascript calls for older MS browsers (modernizr comes later)-->
  <!--[if lt IE 9]><script src="js/thirdparty/html5shiv/html5shiv.js"></script><![endif]-->
  <!--[if (gte IE 6)&(lte IE 8)]><script src="js/selectivizr.min.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="js/IE8.combined.min.js"></script><![endif]-->
  <style type="text/css">.non-div,.non-div2{display: inline;}a,a:visited{
    color:#4a4a4a;}</style>
</head>

<body>

  <div class="content">
<nav>

<?php

$defaults = array(
  'theme_location'  => '',
  'menu'            => '',
  'container'       => '',
  'container_class' => '',
  'container_id'    => '',
  'menu_class'      => 'non-div',
  'menu_id'         => '',
  'exclude'         => '5,11,13,17,54,109',
  'echo'            => true,
  'fallback_cb'     => 'wp_page_menu',
  'before'          => '',
  'after'           => '',
  'link_before'     => '',
  'link_after'      => '',
  'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
  'depth'           => 0,
  'walker'          => ''
);

wp_nav_menu( $defaults );

?>

<a href="http://www.permanentinc.co.nz/bluesteel/" class="logo"></a>

<?php

$defaults2 = array(
  'theme_location'  => '',
  'menu'            => '',
  'container'       => '',
  'container_class' => '',
  'container_id'    => '',
  'menu_class'      => 'non-div2',
  'menu_id'         => '',
  'exclude'         => '13,15,7,9,54,19,109',
  'echo'            => true,
  'fallback_cb'     => 'wp_page_menu',
  'before'          => '',
  'after'           => '',
  'link_before'     => '',
  'link_after'      => '',
  'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
  'depth'           => 0,
  'walker'          => ''
);

wp_nav_menu( $defaults2 );

?>


</nav>
<!--
    <nav>
     <ul>
      <li><a href="http://localhost/BlueSteel/WWW/song-list/">Song List</a></li>
      <li><a href="http://localhost/BlueSteel/WWW/audio/">Audio</a></li>
      <li><a href="http://localhost/BlueSteel/WWW/video/">Video</a></li>
      <li><a href="http://localhost/BlueSteel/WWW/contact/">Contact</a></li>
    </ul>
    <a href="http://localhost/BlueSteel/WWW/" class="logo"></a>
    <ul>
      <li><a href="http://localhost/BlueSteel/WWW/about/">About</a></li>
      <li><a href="http://localhost/BlueSteel/WWW/gallery/">Gallery</a></li>
      <li><a href="http://localhost/BlueSteel/WWW/testimonials/">Testimonials</a></li>
      <li><a href="http://localhost/BlueSteel/WWW/clients/">Clients</a></li>
    </ul>
  </nav> -->

  <div class="wrap">