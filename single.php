<?php include 'header.php'; ?>

<article class="row">
	<section class="publish-date"><span><?php the_time('j') ?></span><span><?php the_time('M') ?></span></section>
	<h1><?php the_title(); ?></h1>

	<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$url = $thumb['0'];
	if($url != null){ ?>
	<a href="<?php the_permalink() ?>" class="news-image" data-image="<?php echo $url; ?>"></a>
	<?php } else{?>
	<div class="gap"></div>
	<?php }?>
	<p><?php echo $post->post_content;?></p>
</article>

<?php include 'footer.php'; ?>
