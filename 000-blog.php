<?php /* Template name: Blog */ ?>

<?php include 'header.php'; ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

  <?php the_content(); ?>

<?php endwhile; ?>

<?php $args = array(
  'posts_per_page'   => 5,
  'offset'           => 0,
  'category'         => '',
  'orderby'          => 'post_date',
  'order'            => 'DESC',
  'include'          => '',
  'exclude'          => '',
  'meta_key'         => '',
  'meta_value'       => '',
  'post_type'        => 'post',
  'post_mime_type'   => '',
  'post_parent'      => '',
  'post_status'      => 'publish',
  'suppress_filters' => true ); ?>

  <?php $the_query = new WP_Query( $args ); ?>

  <?php if ( $the_query->have_posts() ) : ?>

    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
      <?php the_time(); ?>
      <?php the_title(); ?>

      <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
      $url = $thumb['0']; ?>

      <?php echo substr(strip_tags($post->post_content), 0, 500);?>
      <?php the_permalink() ?>

    <?php endwhile; ?>

    <?php wp_reset_postdata(); ?>

  <?php endif; ?>

  <?php include 'footer.php';?>
