In an attempt to incorporate a more semantic colour naming conevntion I came across this post by [Tom Davies](http://erskinedesign.com/blog/friendlier-colour-names-sass-maps/) providing a much cleaner approach.

Instead of using arbitrary names that are not descriptive enough or lack structure, we define a base colour and then add on variants of the following nature:

`x-dark`
`dark`
`mid-dark`
`base (default)`
`mid-light`
`light`
`x-light`


So for a set of blues it would end up like so: 

    $blue-x-dark:#6EBEEA;
    $blue-dark:#6EBEEA;
    $blue-mid-dark: #C9E2F1;
    $blue-base: #1F5F7F;
    $blue-mid-light: #E8F2F8;
    $blue-light:#3092CF;
    $blue-x-light:#3092CF;

This will save you from having to scratch your head over what you called a particular colour and if you are slightly off it is easy enough to traverse the tones attributed to a colour to reach the right shade that you are looking for. 