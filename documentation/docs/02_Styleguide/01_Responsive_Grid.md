The grid system system utilises 12 columns, conforming to the `$large` breakpoint constraints.

Below the `$small` breakpoint, the columns become fluid and stack vertically.

<br>
####Basic grid HTML

For a simple two column layout, create a `.row` and add the appropriate number of `.grid--*` columns. As this is a 12-column grid, each `.grid--*` spans a number of those 12 columns, and should always add up to 12 for each row.

    <div class="row">
      <div class="grid--6">...</div>
      <div class="grid--6">...</div>
    </div>

<br>
####Floating
If you want to float a `.grid--*` element to the right then you can add in the additonal class `.grid--right`. This is useful if you have a smaller column inside of a row that needs positioning. 
    
    <div class="row">
      <div class="grid--6 grid--right">...</div>
    </div>

<br>
####Centering
If you want to canter a `.grid--*` element, then add in the class `.grid--right`. This is useful if you have a smaller columninside of a row that needs centering. 
.grid--center

    div class="row">
     <div class="grid--6 grid--center">...</div>
    /div>

<br>
####Mobile Alternatives
The grid used in basecamp is similar to most but does allow you to include a mobile version of the grid created to force widths on resize.

`.grid--mobile-half`<br>
`.grid--mobile-third`<br>
`.grid--mobile-quater`<br>

    <div class="row">
      <div class="grid--6 grid--mobile-half">...</div>
      <div class="grid--6 grid--mobile-half">...</div>
    </div>    
These will be instantiated by your `$medium` breakpoint and revert to a full width layout when you reach the `$small` breakpoint.

<br>
####Breakpoints
Application breakpoints are defined inside of *_variables.scss* and directly impact the grid system.

`$x-small: 480px`
`$small:   640px`
`$medium:  768px`
`$large:   940px`
`$x-large: 1280px`

The included breakpoint variables also have hidden and visible states related to themselves. 

`.visible--phone`<br>
`.visible--tablet`<br>
`.visible--desktop`<br>
<br>
`.hidden--phone`<br>
`.hidden--tablet`<br>
`.hidden--desktop`<br>

<br><br>
