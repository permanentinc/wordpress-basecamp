/**
 *
 * Gulp task automation
 * Author Jaydn de Graaf Esquire
 * Permanentnic@gmail.com
 *
 */

 'use strict';

// Paths and variables
var basePath = 'app/';
var paths = {
    images: {
        src: basePath + 'images/sprites',
        dest: basePath + 'images/'
    },
    sprite: {
        src: basePath + 'images/sprites/png/*'
    },
    styles: {
        src: basePath + 'styles/components/',
        dest: basePath + 'styles/'
    }
};

// Sprite config
var spriteConfig = {
    imgName: 'sprites.png',
    cssName: '_sprites.scss',
    imgPath: '../images/sprites.png'
};

// Include Gulp
var gulp = require('gulp');
var es = require('event-stream');
var gutil = require('gulp-util');

// Gulp plugin definitions
var plugins = require("gulp-load-plugins")({
    pattern: ['gulp-*', 'gulp.*'],
    replaceString: /\bgulp[\-.]/
});

// Watch config
var appFiles = {
    styles: paths.styles.src + '**/*.scss'
};

// Vendor variables
var vendorFiles = {
    styles: '',
    scripts: ''
};

// Project declarations
var isProduction = true;
var sassStyle = 'expanded';
var sourceMap = false;

// Project environment
if (gutil.env.dev === true) {
    sassStyle = 'expanded';
    sourceMap = true;
    isProduction = false;
}

// Watch message and file changes
var changeEvent = function (evt) {
    var str = evt.path;
    var parsedFile = str.substring(str.lastIndexOf("\\") + 1);
    gutil.log(gutil.colors.yellow('File'), gutil.colors.yellow(parsedFile), gutil.colors.yellow('was'), gutil.colors.yellow(evt.type));
};

// Sprite generation
gulp.task('sprite', function () {
    var spriteData = gulp.src(paths.sprite.src).pipe(plugins.spritesmith({
        imgName: spriteConfig.imgName,
        cssName: spriteConfig.cssName,
        cssFormat: 'css',
        imgPath: spriteConfig.imgPath,
        cssVarMap: function (sprite) {
            sprite.name = sprite.name;
        }
    }));
    spriteData.img.pipe(gulp.dest(paths.images.dest));
    spriteData.css.pipe(gulp.dest(paths.styles.src + 'utilities/'));
});

// Stylesheet compilation
gulp.task('scss', function () {
    var sassFiles = gulp.src(appFiles.styles)
    .pipe(plugins.rubySass({
        style: sassStyle, sourcemap: sourceMap, precision: 10
    }))
    .on('error', function (err) {
        new gutil.PluginError('CSS', err, {showStack: true});
    });
    return es.concat(gulp.src(vendorFiles.styles), sassFiles)
    .pipe(plugins.concat('main.css'))
    .pipe(plugins.autoprefixer('last 3 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    //TODO Need to find out a way of working with duplicated values for the REM fallback
    // .pipe(isProduction ? plugins.combineMediaQueries() : gutil.noop())
    // .pipe(isProduction ? plugins.cssmin(['skip-aggressive-merging']) : gutil.noop())
    .pipe(plugins.size({title: 'Compiled CSS size →'}))
    .pipe(plugins.size({title: '                  →', gzip: true}))
    .pipe(gulp.dest(paths.styles.dest));
});

gulp.task('default', ['sprite', 'scss'], function () {
    gulp.watch(appFiles.styles, ['scss']).on('change', function (evt) {
        changeEvent(evt);
    });
});

// Browser sync functionality

var browserSync = require('browser-sync');

gulp.task('browser-sync', function () {
    browserSync.init(["**/*.css", "**/*.html"], {
        server: {
            baseDir: "app/"
        }
    });
});

gulp.task('serve', ['default', 'browser-sync'], function () {
    gulp.watch(appFiles.styles, ['scss']).on('change', function (evt) {
        changeEvent(evt);
    });
});


gulp.task('stylestats', function () {
  gulp.src('app/styles/*.css')
  .pipe(plugins.stylestats({
    "gzippedSize": true
  }));
});

gulp.task('stats', ['stylestats']);