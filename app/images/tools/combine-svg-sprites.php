#!/usr/bin/php
<?php

class CombineSVGSprites
{

    private static $successMessages = [
        'Great success!',
        'Very nice-uh!',
        'You win 5 internets.',
        'Press the any key to continue.',
        "It's super effective!",
        'The force is strong with this one.'
    ];
    private static $defaultConfigFileName = 'svg-sprite-config.json';
    private        $config;
    /**
     * @var Sprite[]
     */
    private $sprites = [];

    public function __construct() {
        $options = getopt('', [
            'config-path::'
        ]);
        $this->loadConfig(!empty($options['config-path']) ? $options['config-path'] : self::$defaultConfigFileName);
    }

    /**
     * Create a configuration from a json file at the specified path
     *
     * @param $filePath
     *
     * @throws InvalidArgumentException
     */
    private function loadConfig($filePath) {
        $config = Config::fromData(file_get_contents($filePath));
        if (!$config instanceof Config) {
            throw new InvalidArgumentException('The configuration file could not be loaded.');
        } else {
            $this->config = $config;
        }
    }

    public function run() {
        $this->loadSpritesInPaths($this->getConfig('sourcePath'));
        $collatedSprites = $this->collateSprites();
        $this->write($collatedSprites);

        $count  = count($this->sprites);
        $plural = $count == 1 ? '' : 's';
        echo "$count sprite$plural combined." . PHP_EOL;
        echo $this->getRandomSuccessMessage() . PHP_EOL;
    }

    /**
     * Loads all sprites in the given paths
     *
     * @param string|array $paths Globs for the paths to load sprites from
     */
    private function loadSpritesInPaths($paths) {
        $paths = (array)$paths;
        $files = [];
        foreach ($paths as $path) {
            $files = array_merge($files, glob($path));
        }
        $files = array_keys(array_flip($files));
        if (empty($files)) {
            die ('No sprites found in any of these paths: \'' .
                 implode("', '", $this->getConfig('sourcePath')) . '\'.');
        }
        foreach ($files as $file) {
            $sprite = $this->loadSprite($file);
            if ($sprite instanceof Sprite) {
                $this->sprites[] = $sprite;
            }
        }
    }

    public function getConfig($name) {
        if (isset($this->config) && property_exists($this->config, $name)) {
            return $this->config->$name;
        } else {
            return null;
        }
    }

    public function setConfig($name, $value) {
        if (!isset($this->config)) {
            $this->config = new Config();
        }
        if (property_exists($this->config, $name)) {
            $this->config->$name = $value;
        }
    }

    /**
     * @param string $filePath
     *
     * @return Sprite|false
     */
    private function loadSprite($filePath) {
        $contents = json_decode(str_replace('  ', ' ', file_get_contents($filePath)));

        if ($contents) {
            $sprite          = new Sprite();
            $sprite->id      = $contents->groupID;
            $sprite->defs    = $contents->defs;
            $sprite->content = $contents->group;
            $sprite->html    = $contents->html;

            return $sprite;
        }

        return false;
    }

    /**
     * Combines all the currently loaded sprites into an array with svg headers, footers, and grouped definitions
     *
     * @return string[]
     */
    private function collateSprites() {
        $output   = [];
        $output[] = $this->getConfig('before');

        $output[] = "\t" . $this->getConfig('defsOpen');
        foreach ($this->sprites as $sprite) {
            if ($sprite->defs) {
                $output[] = $sprite->defs;
            }
        }
        $output[] = "\t" . $this->getConfig('defsClose');

        foreach ($this->sprites as $sprite) {
            $content = trim($sprite->content);
            if ($content) {
                $output[] = "\t" . str_replace('$id', $sprite->id, $this->getConfig('groupOpen'));
                $output[] = $sprite->content;
                $output[] = "\t" . $this->getConfig('groupClose');
            }
        }

        $output[] = $this->getConfig('after');

        return $output;
    }

    /**
     * @param string $output
     */
    private function write($output) {
        $this->writeFile($this->getConfig('outputPath'), implode(PHP_EOL, $output));
        $this->outputHtmlFiles();
    }

    private function writeFile($filename, $content) {
        if ($this->getConfig('outputMode') == 'compressed') {
            $content = preg_replace('`(?:(?<=[>"])\s+(?=[</>])|\s{2,})`', '', $content);
        }

        return file_put_contents($filename, $content);
    }

    /**
     * Outputs each sprite's HTML to its own file in the specified target location (if any)
     */
    private function outputHtmlFiles() {
        $outputPath      = $this->getConfig('htmlOutputPath');
        $outputExtension = $this->getConfig('htmlOutputExtension');
        if (!($outputPath and $outputExtension)) {
            return;
        }

        if (!file_exists($outputPath)) {
            mkdir($outputPath, 0777, true);
        } elseif (!is_dir($outputPath)) {
            echo 'Unable to write HTML sprites; output path is a file (should be a directory)' . PHP_EOL;

            return;
        }
        foreach ($this->sprites as $sprite) {
            $filename = $this->bemToCamelCase($sprite->id);
            $this->writeFile($outputPath . DIRECTORY_SEPARATOR . $filename . '.' . $outputExtension, $sprite->html);
        }
    }

    /**
     * Convert something like 'header-button-globe--active' to 'HeaderButtonGlobe_active'
     *
     * @param string $input
     *
     * @return mixed
     */
    private function bemToCamelCase($input) {
        $output = str_replace(['--', '-'], ['_', ' '], $input);
        $output = str_replace(' ', '', ucwords($output));

        return $output;
    }

    /**
     * Gets a random message from {@link $successMessages}
     *
     * @return string
     */
    private function getRandomSuccessMessage() {
        return self::$successMessages[array_rand(self::$successMessages)];
    }

}

class Sprite
{

    public $id;
    public $defs;
    public $content;
    public $html;
}

class Config
{

    private static $jsonMap             = [
        'source-path'           => 'sourcePath',
        'output-path'           => 'outputPath',
        'before'                => 'before',
        'after'                 => 'after',
        'output-mode'           => 'outputMode',
        'defs-open-tag'         => 'defsOpen',
        'defs-close-tag'        => 'defsClose',
        'group-open-tag'        => 'groupOpen',
        'group-close-tag'       => 'groupClose',
        'html-output-path'      => 'htmlOutputPath',
        'html-output-extension' => 'htmlOutputExtension'
    ];

    public $sourcePath;
    public $outputPath;
    public         $before              = '<svg xmlns="http://www.w3.org/2000/svg" width="0" height="0" overflow="hidden">';
    public         $after               = '</svg>';
    public         $defsOpen            = '<defs>';
    public         $defsClose           = '</defs>';
    public         $groupOpen           = '<g id="$id">';
    public         $groupClose          = '</g>';
    public         $outputMode          = 'normal';
    public         $htmlOutputPath      = null;
    public         $htmlOutputExtension = 'html';

    /**
     * Factory to create a new Config object from json input (either a json string, array, or stdClass)
     *
     * @param string|string[]|stdClass $data
     *
     * @return static
     */
    public static function fromData($data) {
        if (!is_array($data)) {
            if ($data instanceof stdClass) {
                $data = (array)$data;
            } else {
                $data = json_decode($data, true);
            }
        }
        $config = new static;
        foreach (self::$jsonMap as $k => $v) {
            if (isset($data[$k])) {
                $config->$v = $data[$k];
            }
        }

        return $config;
    }

}

$combiner = new CombineSVGSprites();
$combiner->run();
