///<reference path="definitions/node.d.ts"/>
var CombineSVGSprites = (function () {
    function CombineSVGSprites() {
        this.pathHelper = require('path');
        this.fs = require('fs');
        /**
        * @var Sprite[]
        */
        this.sprites = [];
        if (process.argv.length > 2) {
            // Remove script name from args
            var args = process.argv.splice(2);
            for (var i in args) {
                if (args.hasOwnProperty(i) && args[i].substr(0, 14) == '--config-path=') {
                    this.currentPath = args[i].replace('--config-path=', '');
                }
            }
        }
        this.loadConfig(this.currentPath || this.pathHelper.join(process.cwd(), CombineSVGSprites.defaultConfigFileName));
        this.currentPath = process.cwd();
    }
    /**
    * Create a configuration from a json file at the specified path
    *
    * @param {string} filePath
    */
    CombineSVGSprites.prototype.loadConfig = function (filePath) {
        this.config = Config.fromData(this.fs.readFileSync(filePath).toString());
    };

    CombineSVGSprites.prototype.run = function () {
        this.loadSpritesInPaths(this.getConfig('sourcePath'));
        var collatedSprites = this.collateSprites();
        this.write(collatedSprites);

        var count = this.sprites.length, plural = count == 1 ? '' : 's';
        console.log(count + ' sprite' + plural + ' combined.');
        console.log(CombineSVGSprites.getRandomSuccessMessage());
    };

    /**
    * Loads all sprites in the given paths
    *
    * @param paths string[]|string Globs for the paths to load sprites from
    */
    CombineSVGSprites.prototype.loadSpritesInPaths = function (paths) {
        if (typeof paths === 'string') {
            paths = [paths];
        }
        var files = [];
        for (var i = 0; i < paths.length; i++) {
            var path = paths[i], glob = require('glob'), spriteSourcePath = this.pathHelper.resolve(this.currentPath, path);
            files = files.concat(glob.sync(spriteSourcePath));
        }

        for (i = 0; i < files.length; ++i) {
            for (var j = i + 1; j < files.length; ++j) {
                if (files[i] === files[j]) {
                    files.splice(j--, 1);
                }
            }
        }
        if (files.length === 0) {
            throw 'No sprites found in any of these paths: \'' + this.getConfig('sourcePath').join("', '") + '\'.';
        }
        for (var j = 0; j < files.length; j++) {
            var file = files[j], sprite = this.loadSprite(file);
            if (sprite instanceof Sprite) {
                this.sprites.push(sprite);
            }
        }
    };

    /**
    * @param filePath string
    *
    * @return Sprite
    */
    CombineSVGSprites.prototype.loadSprite = function (filePath) {
        var contents = JSON.parse(this.fs.readFileSync(filePath).toString().replace(/\s{2}/g, ' '));

        if (contents) {
            var sprite = new Sprite();
            sprite.id = contents.groupID;
            sprite.defs = contents.defs;
            sprite.content = contents.group;
            sprite.html = contents.html;

            return sprite;
        }

        return null;
    };

    CombineSVGSprites.prototype.getConfig = function (name) {
        if (this.config !== undefined && this.config[name] !== undefined) {
            return this.config[name];
        } else {
            return null;
        }
    };

    CombineSVGSprites.prototype.setConfig = function (name, value) {
        if (this.config === undefined) {
            this.config = new Config();
        }
        if (this.config[name] !== undefined) {
            this.config[name] = value;
        }
    };

    /**
    * Combines all the currently loaded sprites into an array with svg headers, footers, and grouped definitions
    *
    * @return string[]
    */
    CombineSVGSprites.prototype.collateSprites = function () {
        var output = [];
        output.push(this.getConfig('before'));

        output.push('\t' + this.getConfig('defsOpen'));
        for (var i = 0; i < this.sprites.length; i++) {
            var sprite = this.sprites[i];
            if (sprite.defs) {
                output.push(sprite.defs);
            }
        }
        output.push('\t' + this.getConfig('defsClose'));

        for (var j = 0; j < this.sprites.length; j++) {
            var sprite = this.sprites[j], content = sprite.content.trim();
            if (content) {
                output.push('\t' + this.getConfig('groupOpen').replace(/\$id/g, sprite.id));
                output.push(sprite.content);
                output.push('\t' + this.getConfig('groupClose'));
            }
        }

        output.push(this.getConfig('after'));

        return output;
    };

    /**
    * @param output string
    */
    CombineSVGSprites.prototype.write = function (output) {
        this.writeFile(this.getConfig('outputPath'), output.join('\n'));
        this.outputHtmlFiles();
    };

    CombineSVGSprites.prototype.writeFile = function (filename, content) {
        if (this.getConfig('outputMode') == 'compressed') {
            content = content.replace(/>\s+</g, '><').replace(/"\s+([/>])/g, '"$1').replace(/\s{2,}/g, ' ');
        }

        var outputDir = this.pathHelper.dirname(filename);
        if (!this.fs.existsSync(outputDir)) {
            this.mkdirSync(outputDir);
        }

        return this.fs.writeFileSync(filename, content);
    };

    CombineSVGSprites.prototype.mkdirSync = function (path, mode, position) {
        var osSep = this.pathHelper.sep, parts = require('path').normalize(path).split(osSep);

        mode = mode || process.umask();
        position = position || 0;

        if (position >= parts.length) {
            return true;
        }

        var directory = parts.slice(0, position + 1).join(osSep) || osSep;
        try  {
            this.fs.statSync(directory);
            this.mkdirSync(path, mode, position + 1);
        } catch (e) {
            try  {
                this.fs.mkdirSync(directory, mode);
                this.mkdirSync(path, mode, position + 1);
            } catch (e) {
                if (e.code != 'EEXIST') {
                    throw e;
                }
                this.mkdirSync(path, mode, position + 1);
            }
        }
    };

    /**
    * Outputs each sprite's HTML to its own file in the specified target location (if any)
    */
    CombineSVGSprites.prototype.outputHtmlFiles = function () {
        var outputPath = this.getConfig('htmlOutputPath'), outputExtension = this.getConfig('htmlOutputExtension');
        if (!(outputPath && outputExtension)) {
            return;
        }

        for (var i = 0; i < this.sprites.length; i++) {
            var sprite = this.sprites[i], filename = CombineSVGSprites.bemToCamelCase(sprite.id);
            this.writeFile(this.pathHelper.join(outputPath, filename + '.' + outputExtension), sprite.html);
        }
    };

    CombineSVGSprites.ucwords = /**
    * Equivalent of php's ucwords function
    * @param str
    * @returns {string}
    */
    function (str) {
        return (str + '').replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function ($1) {
            return $1.toUpperCase();
        });
    };

    CombineSVGSprites.bemToCamelCase = /**
    * Convert something like 'header-button-globe--active' to 'HeaderButtonGlobe_active'
    *
    * @param input string
    *
    * @return mixed
    */
    function (input) {
        var output = input.replace(/--/g, '_').replace(/-/g, ' ');
        output = CombineSVGSprites.ucwords(output).replace(/\s/g, '');

        return output;
    };

    CombineSVGSprites.getRandomSuccessMessage = /**
    * Gets a random message from {@link successMessages}
    *
    * @return string
    */
    function () {
        var index = Math.floor(Math.random() * CombineSVGSprites.successMessages.length);
        return CombineSVGSprites.successMessages[index];
    };
    CombineSVGSprites.successMessages = [
        'Great success!',
        'Very nice-uh!',
        'You win 5 internets.',
        'Press the any key to continue.',
        "It's super effective!",
        'The force is strong with this one.'
    ];

    CombineSVGSprites.defaultConfigFileName = 'svg-sprite-config.json';
    return CombineSVGSprites;
})();

var Sprite = (function () {
    function Sprite() {
    }
    return Sprite;
})();

var Config = (function () {
    function Config() {
        this.before = '<svg xmlns="http://www.w3.org/2000/svg" width="0" height="0" overflow="hidden">';
        this.after = '</svg>';
        this.defsOpen = '<defs>';
        this.defsClose = '</defs>';
        this.groupOpen = '<g id="$id">';
        this.groupClose = '</g>';
        this.outputMode = 'normal';
        this.htmlOutputPath = null;
        this.htmlOutputExtension = 'html';
    }
    Config.fromData = /**
    * Factory to create a new Config object from json input (either a json string, array, or stdClass)
    *
    * @param data
    *
    * @return static
    */
    function (data) {
        if (typeof data === 'string') {
            data = JSON.parse(data);
        }

        var config = new Config(), map = Config.jsonMap;
        for (var i in map) {
            if (!map.hasOwnProperty(i)) {
                continue;
            }
            var v = map[i];
            if (data[i]) {
                config[v] = data[i];
            }
        }

        return config;
    };
    Config.jsonMap = {
        'source-path': 'sourcePath',
        'output-path': 'outputPath',
        'before': 'before',
        'after': 'after',
        'output-mode': 'outputMode',
        'defs-open-tag': 'defsOpen',
        'defs-close-tag': 'defsClose',
        'group-open-tag': 'groupOpen',
        'group-close-tag': 'groupClose',
        'html-output-path': 'htmlOutputPath',
        'html-output-extension': 'htmlOutputExtension'
    };
    return Config;
})();

var combiner = new CombineSVGSprites();
combiner.run();
//# sourceMappingURL=combine-svg-sprites.js.map
